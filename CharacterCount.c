#include <stdio.h>

int CountCharacterOccurrence( char inString[], int size, char character )
{
	int occurrence = 0;

	//TODO: count the occurrence of character in inString

	return occurrence;
}

int main( int argc, char** argv )
{
	//Declare an array of 10 elements
	char text[32] = "Llanfairpwllgwyngyll";

	//search for the number of lower case l
	char searchfor = 'l';

	int count = 0;

	count = CountCharacterOccurrence(text, 32, searchfor);

	printf("There are %d %c in %s\n", count, searchfor, text);

	return 0;
}
