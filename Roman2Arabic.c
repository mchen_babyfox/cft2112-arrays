#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Macro for unit-testing
#define TEST(input, expected_result, f)\
    printf("%s should be %d, your result is %d ", input, expected_result, f);\
    if(f == expected_result)\
        printf("PASS!\n");\
    else\
        printf("FAIL!\n");

int RomanToArabic(const char romanNum[])
{
    //Programming Challenge:
    //This function takes a Roman numeral as an array of character (case-insensitive)
    //and converts them into Arabic numeral.
    //The function returns the result as an integer
    //
    //TODO: Complete the implementation
    int result = 0;

    return result;
}

int main(int argc, const char** argv)
{
    //Test cases

    TEST("IX", 9, RomanToArabic("IX"))
    TEST("xlvii", 47, RomanToArabic("xlvii")) 
    TEST("XL", 40, RomanToArabic("XL"))
    TEST("mmxx", 2020, RomanToArabic("mmxx"))
    TEST("MDIV", 1504, RomanToArabic("MDIV"))
    TEST("McDlXiV", 1464, RomanToArabic("McDlXiV"))

    return 0;
}