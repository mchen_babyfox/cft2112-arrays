#include <stdio.h>

int SumArray( int inArray[], int size )
{
	int sum = 0;

	//TODO: calculate the sum of all integers in the array
	//
	
	return sum;
}

float MultArray( float inArray[], int size )
{
	float product = 1.0f;

	//TODO: calculate the product of all floating point values in the array
	//
	
	return product;
}

int SumArrayEven( int inArray[], int size )
{
	int sum = 0;

	//TODO: Calculate the sum of all even integers in the array

	return sum;
}

int MultArrayOdd( int inArray[], int size )
{
	int product = 1;

	//TODO: Calculate the product of all odd integers in the array

	return product;
}

int main( int argc, char** argv )
{
	//Declare an array of 10 elements
	int intArray[10] = {1,2,3,4,5,6,7,8,9,10};
	float floatArray[10] = {0.3, 0.5, 0.31, 0.8, 1.7, 1.5, 10.2, 0.38, 0.1, 3.1415};

	int sum = SumArray(intArray, 10);
	float product = MultArray(floatArray, 10);
	int sum_even = SumArrayEven(intArray, 10);
	int product_odd = MultArrayOdd(intArray, 10);


	printf("The sum is: %d\n", sum);
	printf("The product is: %f\n", product);
	printf("The sum of all even integers is: %d\n", sum_even);
	printf("The product of all odd integers is: %d\n", product_odd);

	return 0;
}
