#include <stdio.h>

int StringLength( char inString[], int size )
{
	int length = 0;

	//TODO: count the number of characters in inString
	//N.B. Do not include the null character!

	return length;
}

void ReverseString( char inString[], int size )
{
	//Get the number of character in inString
	int num_characters = StringLength(inString, size);

	//TODO: Reverse the character in inString
}

int main( int argc, char** argv )
{
	//Declare an array of 10 elements
	char text[10] = "Hello";

	printf("Original: %s\n", text);

	ReverseString(text, 10);

	printf("Reversed: %s\n", text);

	return 0;
}
